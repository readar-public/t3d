# T3D

Within the T3D program funded by the municipality of the Hague, Readar provided a proof of concept for fast mapping of buildings using SLAM technology on consumer devices. This repository aims to inform the municipality and 3rd parties, wether it be public or private, of our findings such that they can be used in further development.

The following key requirements were set:
- a non-technical user should be able to scan a building with his/her smartphone after only a simple instruction
- raw data from the scan should be available to do a desk inspection at a later stage
- the scan should be translated to a floorplan which can be used within the T3D program.

A proof of concept has been developed which shows:
- that requirements can be met when we use Apple ARKit and LiDAR enabled iPhones or Ipads, wether it be via an existing3rd party app or own development
- storing video provides valuable information for desk inspection, however this data is mostly not available while using existing 3rd party apps (as of October 2021)
- floorplans can be generated with an accuracy of 5-15cm
- floorplans showing outer/inner walls can be generated automatically for most common floorplans
- manual quality checks and a workflow for editing the automatically generated floorplan is advisable

We separate three parts in this repository:
1. Datasets
2. Acquisition and pointcloud generation
3. Vectorization

More elaborate documentation is to be found within each folder.
